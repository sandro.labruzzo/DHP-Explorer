package com.sandro.app.fs;

public class MDStoreInfo {
    private String mdstore;
    private String currentId;
    private Long latestTimestamp;


    public MDStoreInfo() {
    }

    public MDStoreInfo(String mdstore, String currentId, Long latestTimestamp) {
        this.mdstore = mdstore;
        this.currentId = currentId;
        this.latestTimestamp = latestTimestamp;
    }

    public String getMdstore() {
        return mdstore;
    }

    public MDStoreInfo setMdstore(String mdstore) {
        this.mdstore = mdstore;
        return this;
    }

    public String getCurrentId() {
        return currentId;
    }

    public MDStoreInfo setCurrentId(String currentId) {
        this.currentId = currentId;
        return this;
    }

    public Long getLatestTimestamp() {
        return latestTimestamp;
    }

    public MDStoreInfo setLatestTimestamp(Long latestTimestamp) {
        this.latestTimestamp = latestTimestamp;
        return this;
    }

    @Override
    public String toString() {
        return "MDStoreInfo{" +
                "mdstore='" + mdstore + '\'' +
                ", currentId='" + currentId + '\'' +
                ", latestTimestamp=" + latestTimestamp +
                '}';
    }
}
