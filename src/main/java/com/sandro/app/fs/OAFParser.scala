package com.sandro.app.fs

import scala.collection.mutable
import scala.xml.MetaData
import scala.xml.pull.{EvElemEnd, EvElemStart, EvText, XMLEventReader}


case class OAFInfo(datasourcePrefix: String ,cobjCategory: String ,openAccess: List[String], identifierTypes: List[String] , hostedBy: List[String], projectid:String) {}

/** @param xml
 */
class OAFParser(xml: XMLEventReader) {

  def extractAttributes(attrs: MetaData, key: String): String = {
    val res = attrs.get(key)
    if (res.isDefined) {
      val s = res.get
      if (s != null && s.nonEmpty)
        s.head.text
      else
        null
    } else null
  }


  def extractStats(): OAFInfo = {
    var currNode: String = null
    var datasourcePrefix: String = null
    var cobjCategory: String= null
    val openAccess: mutable.Set[String] = mutable.Set()
    val identifierTypes: mutable.Set[String] = mutable.Set()
    val hostedBy: mutable.Set[String] = mutable.Set()
    var projectid:String = null
    var node_status:String = null


    while (xml.hasNext) {
      xml.next match {
        case EvElemStart(_, label, attrs, _) =>
          currNode = label
          label match {
            case "datasourceprefix" => node_status = "datasourceprefix"
            case "CobjCategory" => node_status = "CobjCategory"
            case "accessrights" => node_status = "accessrights"
            case "projectid" => node_status = "projectid"
            case "hostedBy" =>
              val it = extractAttributes(attrs, "name")
              if (it != null && it.nonEmpty)
                hostedBy += it
            case "identifier" =>
              val it = extractAttributes(attrs, "identifierType")
              if (it != null && it.nonEmpty)
                identifierTypes += it
            case _           =>
          }
        case EvElemEnd(_, label) =>
          label match {
            case "datasourceprefix" => node_status = null
            case "CobjCategory" => node_status = null
            case "accessrights" => node_status = null
            case "hostedBy" => node_status = null
            case "projectid" => node_status = null
            case _                 =>
          }
        case EvText(text) =>
          if (node_status != null && text.trim.nonEmpty)
            node_status match {
              case "projectid" =>
                projectid = text
              case "datasourceprefix" =>
                datasourcePrefix = text
              case "CobjCategory" =>
                cobjCategory = text
              case "accessrights" =>
                openAccess += text
              case _ =>
            }
        case _ =>
      }
    }
    OAFInfo(datasourcePrefix = datasourcePrefix, cobjCategory = cobjCategory, openAccess = openAccess.toList, identifierTypes = identifierTypes.toList, hostedBy = hostedBy.toList,projectid)
  }
}
