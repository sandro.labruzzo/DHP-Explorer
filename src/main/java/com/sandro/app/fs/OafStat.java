package com.sandro.app.fs;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class OafStat implements Serializable {

    private String datasourcePrefix;
    private String cobjCategory;
    private  List<String> openAccess;
    private  List<String> identifierTypes;
    private  List<String> hostedBy;


    public String getDatasourcePrefix() {
        return datasourcePrefix;
    }

    public OafStat setDatasourcePrefix(String datasourcePrefix) {
        this.datasourcePrefix = datasourcePrefix;
        return this;
    }

    public String getCobjCategory() {
        return cobjCategory;
    }

    public OafStat setCobjCategory(String cobjCategory) {
        this.cobjCategory = cobjCategory;
        return this;
    }

    private void add_value_to_list(final String value, final List<String> l) {
        if (value==null || StringUtils.isEmpty(value))
            return;
        final String  normalized_value = value.toLowerCase().trim();
        if (l.stream().anyMatch(s-> s.equalsIgnoreCase(normalized_value))){
            l.add(normalized_value);
        }
    }

    public List<String> getOpenAccess() {
        return openAccess;
    }

    public List<String> getIdentifierTypes() {
        return identifierTypes;
    }

    public List<String> getHostedBy() {
        return hostedBy;
    }

    public void addIdentifierType(final String value) {
        if (identifierTypes== null)
            identifierTypes = new ArrayList<>();
        add_value_to_list(value, identifierTypes);
    }

    public void addOpenAccess(final String value) {
        if (openAccess == null)
            openAccess= new ArrayList<>();
        add_value_to_list(value, openAccess);
    }

    public void addHostedBy(final String value) {
        if (hostedBy == null)
            hostedBy = new ArrayList<>();
        add_value_to_list(value, hostedBy);
    }

    public OafStat setOpenAccess(List<String> openAccess) {
        this.openAccess = openAccess;
        return this;
    }

    public OafStat setIdentifierTypes(List<String> identifierTypes) {
        this.identifierTypes = identifierTypes;
        return this;
    }

    public OafStat setHostedBy(List<String> hostedBy) {
        this.hostedBy = hostedBy;
        return this;
    }
}
