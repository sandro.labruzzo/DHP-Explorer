package com.sandro.app

import scala.collection.mutable

object SparkUtility {

  def parseArguments(args: Array[String]): mutable.Map[String, String] = {
    var currentVariable: String = null
    val argumentMap: mutable.Map[String, String] = mutable.Map()

    args.zipWithIndex.foreach {
      case (x, i) =>
        if (i % 2 == 0) {
          // ERROR in case the syntax is wrong
          if (!x.startsWith("-")) throw new IllegalArgumentException("wrong input syntax expected -variable_name value")

          if (x.startsWith("--"))
            currentVariable = x.substring(2)
          else
            currentVariable = x.substring(1)
        }
        else argumentMap += (currentVariable -> x)
    }
    argumentMap
  }

}
