package eu.dnetlib.scholix

import com.sandro.app.fs.{OAFInfo, OAFParser}
import eu.dnetlib.dhp.schema.common.ModelConstants
import eu.dnetlib.dhp.schema.oaf.utils.OafMapperUtils
import eu.dnetlib.dhp.schema.oaf.{DataInfo, KeyValue, Relation, Result}
import eu.dnetlib.dhp.sx.graph.scholix.ScholixUtils.DATE_RELATION_KEY

import scala.io.Source
import scala.xml.pull.XMLEventReader
import org.json4s.DefaultFormats
import org.json4s.JsonAST.{JField, JObject, JString}
import org.json4s.jackson.JsonMethods.parse

import collection.JavaConverters._

case class Measurement(name:String, nsprefix:String, timestamp:Long, value:Long) {}

object DHPUtils {


  val DATA_INFO: DataInfo = OafMapperUtils.dataInfo(
    false,
    null,
    false,
    false,
    ModelConstants.PROVENANCE_ACTION_SET_QUALIFIER,
    "0.9"
  )


  val relations = Map(
    "IsSupplementTo"->"IsSupplementedBy",
    "IsSupplementedBy"->"IsSupplementTo",
    "References"->"IsReferencedBy",
    "IsReferencedBy"->"References",
    "IsRelatedTo"->"IsRelatedTo"  )


val ElsevierCollectedFrom: KeyValue = OafMapperUtils.keyValue("10|openaire____::8f87e10869299a5fe80b315695296b88", "Elsevier")


  def createInverseRelationships(r:Relation): List[Relation] = {

    val inverse = new Relation()
    inverse.setDataInfo(r.getDataInfo)
    inverse.setCollectedfrom(r.getCollectedfrom)
    inverse.setProperties(r.getProperties)
    inverse.setSource(r.getTarget)
    inverse.setTarget(r.getSource)
    inverse.setRelType(r.getRelType)
    inverse.setSubRelType(r.getSubRelType)
    inverse.setRelClass(relations.getOrElse(r.getRelClass, r.getRelClass))
    List(r, inverse)

  }


  def extractPidMap(r:Result):List[(String, String)] = {
    if (r == null || r.getInstance()==null)
      return null
    r.getInstance().asScala.filter(i => i.getPid!= null).flatMap(i =>i.getPid.asScala).map(p => (r.getId, generate_unresolved_id(p.getValue, p.getQualifier.getClassid))).toList
  }

  def extractIdRel(input:String):String = {
    implicit lazy val formats: DefaultFormats.type = org.json4s.DefaultFormats
    lazy val json: org.json4s.JValue = parse(input)

    val relName: String = (json \ "RelationshipType" \ "Name").extract[String]
    val sourcePid = (json \ "Source" \ "Identifier" \ "ID").extract[String]
    val sourcePidType = (json \ "Source" \ "Identifier" \ "IDScheme").extract[String]
    val targetPid = (json \ "Target" \ "Identifier" \ "ID").extract[String]
    val targetPidType = (json \ "Target" \ "Identifier" \ "IDScheme").extract[String]
     s"$sourcePid::$sourcePidType::$relName::$targetPid::$targetPidType".toLowerCase()

  }


  def eventDataToRelation(input:String):Relation = {
    implicit lazy val formats: DefaultFormats.type = org.json4s.DefaultFormats
    lazy val json: org.json4s.JValue = parse(input)

    val relName:String = (json \ "RelationshipType" \ "Name").extract[String]
    val sourcePid = (json\ "Source" \ "Identifier"\ "ID").extract[String]
    val sourcePidType = (json\ "Source" \ "Identifier"\ "IDScheme").extract[String]
    val targetPid = (json \ "Target" \ "Identifier" \ "ID").extract[String]
    val targetPidType = (json \ "Target" \ "Identifier" \ "IDScheme").extract[String]
    val date:String = (json\"LinkPublicationDate").extract[String]

    createRelation(generate_unresolved_id(sourcePid, sourcePidType),generate_unresolved_id(targetPid, targetPidType), ElsevierCollectedFrom, "relationship",relName, date)




  }


  def createRelation(

                      sourceId: String,
                      targetId:String,
                      collectedFrom: KeyValue,
                      subRelType: String,
                      relClass: String,
                      date: String
                    ): Relation = {

    val rel = new Relation
    rel.setCollectedfrom(List(ElsevierCollectedFrom).asJava)
    rel.setDataInfo(DATA_INFO)

    rel.setRelType(ModelConstants.RESULT_RESULT)
    rel.setSubRelType(subRelType)
    rel.setRelClass(relClass)

    rel.setSource(sourceId)

    rel.setTarget(targetId)

    val dateProps: KeyValue = OafMapperUtils.keyValue(DATE_RELATION_KEY, date)

    rel.setProperties(List(dateProps).asJava)
    rel.setCollectedfrom(List(collectedFrom).asJava)
    rel

  }


  def generate_unresolved_id(pid: String, pidType: String): String = {
    s"unresolved::${pid.toLowerCase()}::${pidType.toLowerCase()}"
  }

  def convertTOOAFStat(input: String): OAFInfo = {
    val xml = new XMLEventReader(Source.fromString(input))
    val parser = new OAFParser(xml)
    parser.extractStats()
  }

}
