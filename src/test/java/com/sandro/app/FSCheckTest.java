package com.sandro.app;

import com.sandro.app.fs.MDStoreInfo;
import org.junit.jupiter.api.Test;

public class FSCheckTest {


    private MDStoreInfo extractPath(final String path, final String basePath) {

        int res = path.indexOf(basePath);
        if (res >0){

            String[] split = path.substring(res).split("/");
            if (split.length > 2) {
                final String ts = split[split.length -1];
                final String mdStore = split[split.length -2];
                return  new MDStoreInfo(mdStore, null, Long.parseLong(ts));
            }

        }
        return  null;


    }

    @Test
    public void doTest() {

        final String basePath = "/user/sandro.labruzzo/stores/";
        final String path = "hdfs://nameservice1/user/sandro.labruzzo/stores/4a0cddf2-20e9-4558-a3c1-4d20cfecffa8_TURTdG9yZURTUmVzb3VyY2VzL01EU3RvcmVEU1Jlc291cmNlVHlwZQ==/1592574025511";


        System.out.println(extractPath(path,basePath));




    }
}
