package com.sandro.app

import com.sandro.app.fs.OAFParser
import org.junit.jupiter.api.Test

import scala.io.Source
import scala.xml.pull.XMLEventReader

class xmlParser {


  @Test
  def testParse(): Unit = {
    val xml =Source.fromInputStream(getClass.getResourceAsStream("/eu/dnetlib/store/odf.xml")).mkString


    val xml_e = new XMLEventReader(Source.fromString(xml))
    val parser = new OAFParser(xml_e)
    println(parser.extractStats())


  }
}
