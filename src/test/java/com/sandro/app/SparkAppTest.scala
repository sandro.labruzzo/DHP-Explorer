package com.sandro.app


import org.junit.jupiter.api.Test
import org.slf4j.{Logger, LoggerFactory}

class TestApp ( args: Array[String], log: Logger)
  extends AbstractScalaApplication( args: Array[String], log: Logger) {
  /** Here all the spark applications runs this method
   * where the whole logic of the spark node is defined
   */
  override def run(): Unit = {
    println(argumentMap)


  }
}

class SparkAppTest {

  @Test
  def testParsing() :Unit = {
    val log: Logger = LoggerFactory.getLogger(this.getClass.getClass)
    val args = "-key1 value1 --key-2 value2"
    val t:TestApp=new TestApp(args.split(" "),log)
    val m =t.parseArguments(args = args.split(" "))
    println(m)
  }

}
